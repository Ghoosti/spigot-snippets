public class GetArguments {

    public String getArguments(String[] args) {

        StringBuilder builder = new StringBuilder();

        for(int i = 1; i < args.length; i++) {
            builder.append(args[i]).append(" ");
        }

        return builder.toString();
    }

}
